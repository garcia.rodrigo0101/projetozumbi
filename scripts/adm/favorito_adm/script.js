document.addEventListener('DOMContentLoaded', function () {

  const searchForm = document.getElementById('search-form');
  const searchInput = document.querySelector('.search-data');

  searchForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const searchTerm = searchInput.value.trim();
    searchItems(searchTerm);
  });

  searchInput.addEventListener('input', function () {
    const searchTerm = searchInput.value.trim();
    searchItems(searchTerm);
  });

  const logoutLink = document.getElementById('logout-link');

  logoutLink.addEventListener('click', function (event) {
    event.preventDefault();
    Logoff();
  });

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {

      var userId = user.uid;

      var userRef = db.collection('users').doc(userId);

      userRef.get().then(function (doc) {
        if (doc.exists) {
          var favoritos = doc.data().favoritos;
          const papel = doc.data().papel;
          if (papel === 'user') {
            window.location.href = '../../index.html'
            Logoff()
            return
          }
          // Verifique se há favoritos
          if (favoritos && favoritos.length > 0) {
            // Recupere as informações dos documentos favoritados
            var favoritosPromises = favoritos.map(function (favoritoRef) {
              return favoritoRef.get();
            });

            Promise.all(favoritosPromises).then(function (favoritosDocs) {
              favoritosDocs.forEach(function (favoritoDoc) {
                var nome = favoritoDoc.data().nome;
                var data_nascimento = favoritoDoc.data().data_nascimento;
                var data_falecimento = favoritoDoc.data().data_falecimento;
                var biografia = favoritoDoc.data().biografia;
                var url = favoritoDoc.data().url;

                var diplomaCard = document.createElement('div');
                diplomaCard.classList.add('blog-card');

                diplomaCard.innerHTML = `
                  <div class="meta">
                    <div class="photo" style="background-image: url(${url})"></div>
                    <ul class="details">
                      <li class="author">${nome}</li>
                      <li class="date">Nascimento: ${data_nascimento}</li>
                      <li class="date">Morte: ${data_falecimento}</li>
                      <li class="favorito" onclick="Desfavoritar('${favoritoDoc.id}')"><a href="#">Desfavoritar</a></li>
                    </ul>
                  </div>
                  <div class="description">
                    <h1>${nome}</h1>
                    <h2>${data_nascimento}</h2>
                    <p>${biografia}</p>
                  </div>
                `;

                var diplomaContainer = document.getElementById('diploma-container');
                diplomaContainer.appendChild(diplomaCard);
              });
            }).catch(function (error) {
              console.log("Erro ao recuperar os favoritos:", error);
            });
          } else {
            console.log("Nenhum favorito encontrado.");
            var noDataTag = document.createElement('div');
            noDataTag.classList.add('texto-sem-favorito');

            noDataTag.innerHTML = `
              <h1>Nenhum Favorito Encontrado</h1>
            `;

            var diplomaContainer = document.getElementById('diploma-container');
            diplomaContainer.appendChild(noDataTag);
          }
        } else {
          console.log("Documento do usuário não encontrado.");
        }
      }).catch(function (error) {
        console.log("Erro ao recuperar o documento do usuário:", error);
      });
    } else {
      Logoff();
      window.location.href = '../../index.html';
    }
  });
});

function Desfavoritar(docId) {
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var userId = user.uid;
      var docRef = db.collection('diplomas').doc(docId);
      var userRef = db.collection('users').doc(userId);

      userRef.update({
        favoritos: firebase.firestore.FieldValue.arrayRemove(docRef)
      }).then(function () {
        console.log('Documento desfavoritado com sucesso!');
        window.location.reload();
      }).catch(function (error) {
        console.log('Erro ao desfavoritar o documento:', error);
      });
    } else {
      console.log('Usuário não autenticado. Redirecionando para a página de login...');
    }
  });
}

function searchItems(searchTerm) {
  var searchTermLower = searchTerm.toLowerCase();
  var user = firebase.auth().currentUser;

  if (!user) {
    console.log('Usuário não autenticado. Redirecionando para a página de login...');
    return;
  }

  var userId = user.uid;
  var userRef = db.collection('users').doc(userId);

  userRef.get().then(function (doc) {
    if (doc.exists) {
      var favoritos = doc.data().favoritos;
      var favoritosIds = favoritos.map(function (favoritoRef) {
        return favoritoRef.id;
      });

      var diplomasRef = db.collection('diplomas');

      diplomasRef.get().then(function (querySnapshot) {
        var filteredDiplomas = [];

        querySnapshot.forEach(function (doc) {
          if (favoritosIds.includes(doc.id)) {
            filteredDiplomas.push(doc);
          }
        });

        var searchResults = filteredDiplomas.filter(function (doc) {
          var nomeLowercase = doc.data().nome.toLowerCase();
          return nomeLowercase.includes(searchTermLower);
        });

        var diplomaContainer = document.getElementById('diploma-container');
        diplomaContainer.innerHTML = '';

        if (searchResults.length === 0) {
          var noDataTag = document.createElement('div');
          noDataTag.classList.add('texto-sem-favorito');

          noDataTag.innerHTML = `
            <h1>Nenhum Favorito Encontrado</h1>
          `;

          diplomaContainer.appendChild(noDataTag);
        } else {
          searchResults.forEach(function (doc) {
            var nome = doc.data().nome;
            var data_nascimento = doc.data().data_nascimento;
            var data_falecimento = doc.data().data_falecimento;
            var biografia = doc.data().biografia;
            var url = doc.data().url;

            var diplomaCard = document.createElement('div');
            diplomaCard.classList.add('blog-card');

            diplomaCard.innerHTML = `
              <div class="meta">
                <div class="photo" style="background-image: url(${url})"></div>
                <ul class="details">
                  <li class="author">${nome}</li>
                  <li class="date">Nascimento: ${data_nascimento}</li>
                  <li class="date">Morte: ${data_falecimento}</li>
                  <li class="favorito" onclick="Desfavoritar('${doc.id}')"><a href="#">Desfavoritar</a></li>
                </ul>
              </div>
              <div class="description">
                <h1>${nome}</h1>
                <h2>${data_nascimento}</h2>
                <p>${biografia}</p>
              </div>
            `;

            diplomaContainer.appendChild(diplomaCard);
          });
        }
      }).catch(function (error) {
        console.log('Erro ao recuperar os diplomas:', error);
      });
    } else {
      console.log('Documento do usuário não encontrado.');
    }
  }).catch(function (error) {
    console.log('Erro ao recuperar o documento do usuário:', error);
  });
}
