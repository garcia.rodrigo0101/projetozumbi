
var dataAtual = new Date();
var dataFormatada = dataAtual.toISOString().slice(0, 10);
document.getElementById('data').value = dataFormatada;

document.addEventListener('DOMContentLoaded', function () {



    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            db.collection("users").doc(user.uid).get()
                .then((doc) => {
                    if (doc.exists) {
                        const papel = doc.data().papel;
                        if (papel === 'user') {
                            window.location.href = '../../index.html'
                            Logoff()
                            return
                        }
                    } else {
                        console.log("Documento de usuário não encontrado");
                        window.location.href = '../../index.html'
                        Logoff()
                        return
                    }
                })
                .catch((error) => {
                    console.log("Erro ao obter o documento de usuário:", error);
                    window.location.href = '../../index.html'
                    Logoff()
                    return
                });
        }
        else {
            window.location.href = '../../index.html'
            Logoff()
        }
    })
    const logoutLink = document.getElementById('logout-link');

    logoutLink.addEventListener('click', function (event) {
        event.preventDefault();
        Logoff();
    });
})
