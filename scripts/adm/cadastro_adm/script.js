document.addEventListener('DOMContentLoaded', function () {

    firebase.auth().onAuthStateChanged(function (user) {
        console.log(user)
        if (user) {
            console.log(user)
            db.collection("users").doc(user.uid).get()
                .then((doc) => {
                    console.log(doc)
                    if (doc.exists) {
                        console.log(user)
                        const papel = doc.data().papel;

                        if (papel === 'user') {
                            console.log(user)

                            window.location.href = '../../index.html'
                            Logoff()
                            return
                        }
                    } else {
                        console.log("Documento de usuário não encontrado");
                        window.location.href = '../../index.html'
                        Logoff()
                        return
                    }
                })
                .catch((error) => {
                    console.log("Erro ao obter o documento de usuário:", error);
                    window.location.href = '../../index.html'
                    Logoff()
                    return
                });

            const logoutLink = document.getElementById('logout-link');

            logoutLink.addEventListener('click', function (event) {
                event.preventDefault();
                console.log();
                Logoff();
            });
        } else {
            window.location.href = '../../index.html'
            Logoff()
            return
        }
    })

    const logoutLink = document.getElementById('logout-link');
    var dataNascimentoInput = document.getElementById('dataNascimento');
    var dataMorteInput = document.getElementById('dataMorte');


    logoutLink.addEventListener('click', function (event) {
        event.preventDefault();
        Logoff();
    });

    dataNascimentoInput.addEventListener('focus', function () {
        this.type = 'date';
        this.placeholder = '';
    });

    dataNascimentoInput.addEventListener('blur', function () {
        if (this.value === '') {
            this.type = 'text';
            this.placeholder = 'Data de Nascimento';
        }
    });

    dataMorteInput.addEventListener('focus', function () {
        this.type = 'date';
        this.placeholder = '';
    });

    dataMorteInput.addEventListener('blur', function () {
        if (this.value === '') {
            this.type = 'text';
            this.placeholder = 'Data de Falecimento';
        }
    });

});


function createDiploma() {
    event.preventDefault();

    const nome = document.getElementById("nome");
    const dataNascimento = document.getElementById("dataNascimento");
    const dataMorte = document.getElementById("dataMorte");
    const url = document.getElementById("url");
    const descricao = document.getElementById("descricao");
    const nomeLowercase = nome.value.toLowerCase();

    
    if (nome === '') {
        alert('O campo "Nome" não pode estar em branco.');
        return;
    }
    if (nome.length > 100) {
        alert('O campo "Nome" deve ter no máximo 100 caracteres.');
        return false;
    }



    const dataNascimentoValue = dataNascimento.value.trim();
    const dataMorteValue = dataMorte.value.trim();

    // Verificar se as datas estão no formato correto
    const dateFormat = 'YYYY-MM-DD';
    if (dataNascimentoValue !== '' && !moment(dataNascimentoValue, dateFormat, true).isValid()) {
        alert('A data de nascimento não está em um formato válido. Utilize o formato "AAAA-MM-DD".');
        return ;
    }
    if (dataMorteValue !== '' && !moment(dataMorteValue, dateFormat, true).isValid()) {
        alert('A data de falecimento não está em um formato válido. Utilize o formato "AAAA-MM-DD".');
        return;
    }

    const biografiaValue = descricao.value.trim();

    if (biografiaValue === '') {
        alert('O campo "Biografia" não pode estar em branco.');
        return;
    }
    if (biografiaValue.length < 100) {
        alert('O campo "Biografia" deve ter no mínimo 100 caracteres.');
        return ;
    }
    if (biografiaValue.length > 1000) {
        alert('O campo "Biografia" deve ter no máximo 1000 caracteres.');
        return;
    }

 

    var db = firebase.firestore();

    db.collection("diplomas").add({
        nome: nome.value,
        data_nascimento: dataNascimento.value,
        data_falecimento: dataMorte.value,
        url: url.value,
        biografia: descricao.value,
        nomeLowercase: nomeLowercase
    })
        .then(() => {
            alert('Diploma adicionado com sucesso!');
            nome.value = '';
            dataNascimento.value = '';
            dataMorte.value = '';
            url.value = '';
            descricao.value = '';
            nomeLowercase.value = '';
        })
        .catch(function (error) {
            console.error("Erro ao adicionar documento: ", error);
        });

}
