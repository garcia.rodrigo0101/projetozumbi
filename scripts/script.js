




function CadastrarUsuario() {
    event.preventDefault();
    console.log("Cadastra Usuário");

    const email = document.getElementById("email_register").value;
    const password = document.getElementById("password_register").value;
    const papel = document.getElementById("role").value;
    const loginBtn = document.querySelector("label.login");

    firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then((userCredential) => {
            loginBtn.click();
            console.log("Usuário criado com sucesso!!!");
            console.log(userCredential);

            // Obter o ID do usuário recém-criado
            const userId = userCredential.user.uid;

            // Criar um array vazio para armazenar os favoritos
            const favoritos = [];

            // Adicionar o documento do usuário na coleção "users" com os campos "papel" e "favoritos"
            db.collection("users")
                .doc(userId)
                .set({
                    papel: papel,
                    favoritos: favoritos,
                })
                .then(() => {
                    console.log("Documento de usuário criado com sucesso!");
                })
                .catch((error) => {
                    console.log("Erro ao criar documento de usuário:", error);
                });
        })
        .catch((error) => {
            alert("Problemas ao criar usuário!");
            console.log("Problemas ao criar usuário");
            console.log(error);
        });
}



function Autenticar() {
    event.preventDefault();
    const email = document.getElementById('email_login').value;
    const password = document.getElementById('password_login').value;
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
            console.log('Usuário logado com sucesso');
            console.log(userCredential.user);

            // Verificar o papel do usuário no banco de dados
            db.collection("users").doc(userCredential.user.uid).get()
                .then((doc) => {
                    if (doc.exists) {
                        const papel = doc.data().papel;
                        if (papel === 'admin') {
                            // Redirecionar para a página de administração
                            window.location.href = "./html/adm/home_adm.html";
                        } else {
                            // Redirecionar para a página do usuário normal
                            window.location.href = "./html/user/home.html";
                        }
                    } else {
                        // Documento de usuário não encontrado
                        console.log("Documento de usuário não encontrado");
                    }
                })
                .catch((error) => {
                    console.log("Erro ao obter o documento de usuário:", error);
                });
        })
        .catch((error) => {
            alert('Login Recusado. Tente novamente mais tarde...');
            const errorCode = error.code;
            const errorMessage = error.message;
        });
}



function VerificarAutenticacao() {
    event.preventDefault();
    const user = firebase.auth().currentUser;
    if (user) {
        console.log("Usuário logado ", user)
    } else {
        console.log("Usuário não logado")
    }
}

function Logoff() {
    event.preventDefault();
    firebase.auth().signOut().then(() => {
        console.log("Usuário desconectado!")
        window.location.href = '../../index.html';
    }).catch((error) => {
        console.log(error)
        window.location.href = '../../index.html';
    })
}

function RedefinirSenha() {
    event.preventDefault();
    const email = document.getElementById('email_login').value;
    firebase.auth().sendPasswordResetEmail(email)
        .then(() => {
            alert("Email de redefinição de senha enviado!")
            console.log('Email de redefinição de senha enviado!');
        })
        .catch((error) => {
            alert("Erro ao enviar o email de redefinição de senha!")
            console.log('Erro ao enviar o email de redefinição de senha:', error);
        });
}

function DeletarUsuario() {
    event.preventDefault();
    const user = firebase.auth().currentUser;

    if (user) {
        user.delete()
            .then(() => {
                console.log('Usuário deletado com sucesso!');
            })
            .catch((error) => {
                console.log('Erro ao deletar o usuário:', error);
            });
    } else {
        console.log('Nenhum usuário logado.');
    }
}


