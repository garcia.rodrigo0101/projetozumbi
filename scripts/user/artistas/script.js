document.addEventListener('DOMContentLoaded', function () {
  const searchForm = document.getElementById('search-form');
  const searchInput = document.querySelector('.search-data');

  searchForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const searchTerm = searchInput.value.trim();
    searchItems(searchTerm);
  });

  searchInput.addEventListener('input', function () {
    const searchTerm = searchInput.value.trim();
    searchItems(searchTerm);
  });


  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      db.collection("users").doc(user.uid).get()
        .then((doc) => {
          if (doc.exists) {
            const papel = doc.data().papel;
            if (papel === 'admin') {
              window.location.href = '../../index.html'
              Logoff()
              return
            }
          } else {
            console.log("Documento de usuário não encontrado");
            window.location.href = '../../index.html'
            Logoff()
            return
          }
        })
        .catch((error) => {
          console.log("Erro ao obter o documento de usuário:", error);
          window.location.href = '../../index.html'
          Logoff()
          return
        });
    } else {
      window.location.href = '../../index.html'
      Logoff()
      return
    }
  })
  const logoutLink = document.getElementById('logout-link');

  logoutLink.addEventListener('click', function (event) {
    event.preventDefault();
    console.log()
    Logoff();
  });

  var diplomasRef = db.collection('diplomas');

  diplomasRef.get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      var nome = doc.data().nome;
      var data_nascimento = doc.data().data_nascimento;
      var data_falecimento = doc.data().data_falecimento;
      var biografia = doc.data().biografia;
      var url = doc.data().url;

      var diplomaCard = document.createElement('div');
      diplomaCard.classList.add('blog-card');

      diplomaCard.innerHTML = `
            <div class="meta">
            <div class="photo" style="background-image: url(${url})"></div>
            <ul class="details">
              <li class="author">${nome}</li>
              <li class="date">Nascimento: ${data_nascimento}</li>
              <li class="date">Morte: ${data_falecimento}</li>
              <li class="favorito" onclick="Favoritar('${doc.id}'
              )"><a href="#">Favoritar</a></li>
            </ul>
          </div>
        <div class="description">
          <h1>${nome}</h1>
          <h2>${data_nascimento}</h2>
          <p>${biografia}</p>
        </div>
      `;

      var diplomaContainer = document.getElementById('diploma-container');
      diplomaContainer.appendChild(diplomaCard);
    });
  }).catch(function (error) {
    console.log("Erro ao recuperar os dados do Firebase: ", error);
  });
})



function Favoritar(docId) {
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // O usuário está logado, você pode acessar suas informações em user.uid
      var userId = user.uid;

      // Crie uma referência ao documento "diplomas/docID"
      var docRef = db.collection('diplomas').doc(docId);

      // Atualize o campo "favoritos" no documento do usuário com a referência
      var userRef = db.collection('users').doc(userId);
      userRef.update({
        favoritos: firebase.firestore.FieldValue.arrayUnion(docRef)
      }).then(function () {
        console.log('Documento favoritado com sucesso!');
      }).catch(function (error) {
        console.log('Erro ao favoritar o documento:', error);
      });
    } else {
      // O usuário não está logado, redirecione-o para a página de login
      console.log('Usuário não autenticado. Redirecionando para a página de login...');
    }
  });
}


function searchItems(searchTerm) {
  var searchTermLower = searchTerm.toLowerCase();
  var diplomasRef = db.collection('diplomas');

  diplomasRef.orderBy('nomeLowercase').startAt(searchTermLower).endAt(searchTermLower + '\uf8ff').get().then(function (querySnapshot) {
    var diplomaContainer = document.getElementById('diploma-container');
    diplomaContainer.innerHTML = '';

    if (querySnapshot.size === 0) {
      var noDataTag = document.createElement('div');
      noDataTag.classList.add('texto-sem-favorito');

      noDataTag.innerHTML = `
                        <h1>Nenhum Diploma Encontrado</h1>
                        
                        `
      var diplomaContainer = document.getElementById('diploma-container');
      diplomaContainer.appendChild(noDataTag);
    } else {


      querySnapshot.forEach(function (doc) {
        var nome = doc.data().nome;
        var data_nascimento = doc.data().data_nascimento;
        var data_falecimento = doc.data().data_falecimento;
        var biografia = doc.data().biografia;
        var url = doc.data().url;

        var diplomaCard = document.createElement('div');
        diplomaCard.classList.add('blog-card');

        diplomaCard.innerHTML = `
        <div class="meta">
          <div class="photo" style="background-image: url(${url})"></div>
          <ul class="details">
            <li class="author">${nome}</li>
            <li class="date">Nascimento: ${data_nascimento}</li>
            <li class="date">Morte: ${data_falecimento}</li>
            <li class="favorito" onclick="Favoritar('${doc.id}')"><a href="#">Favoritar</a></li>
          </ul>
        </div>
        <div class="description">
          <h1>${nome}</h1>
          <h2>${data_nascimento}</h2>
          <p>${biografia}</p>
        </div>
      `;

        diplomaContainer.appendChild(diplomaCard);
      });
    }
  })
    .catch(function (error) {
      console.log('Erro ao executar a busca no Firebase:', error);
    });
}

