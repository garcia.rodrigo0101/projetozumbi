var slider;
var slides;
var buttons;
var container;

document.addEventListener('DOMContentLoaded', function () {


    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            db.collection("users").doc(user.uid).get()
                .then((doc) => {
                    if (doc.exists) {
                        const papel = doc.data().papel;
                        if (papel === 'admin') {
                            window.location.href = '../../index.html'
                            Logoff()
                            return
                        }
                    } else {
                        console.log("Documento de usuário não encontrado");
                        window.location.href = '../../index.html'
                        Logoff()
                        return
                    }
                })
                .catch((error) => {
                    console.log("Erro ao obter o documento de usuário:", error);
                    window.location.href = '../../index.html'
                    Logoff()
                    return
                });
        } else {
            window.location.href = '../../index.html'
            Logoff()
        }
    })
    container = document.getElementById('container');
    slider = document.getElementById('slider');
    slides = document.getElementsByClassName('slide').length;
    buttons = document.getElementsByClassName('btn');
    checkWidth();
    window.addEventListener("resize", checkWidth);
    const logoutLink = document.getElementById('logout-link');

    logoutLink.addEventListener('click', function (event) {
        event.preventDefault();
        Logoff();
    });

    var diplomasRef = db.collection('diplomas');

    diplomasRef.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            var url = doc.data().url;
            var nome = doc.data().nome;

            var artistaImage = document.createElement('div');
            artistaImage.classList.add('slide');

            artistaImage.innerHTML = `<img src="${url}" alt="${nome}"></img>`;

            var slideContent = document.getElementById('slider');
            slideContent.appendChild(artistaImage);
        });

        slides = document.getElementsByClassName('slide').length;
        slidesCount = slides - slidesPerPage;
        checkWidth();
    }).catch(function (error) {
        console.log("Erro ao recuperar os dados do Firebase: ", error);
    });
});


var currentPosition = 0;
var currentMargin = 0;
var slidesPerPage = 0;
var slidesCount = slides - slidesPerPage;
var containerWidth;
var prevKeyActive = false;
var nextKeyActive = true;

function checkWidth() {
    containerWidth = container.offsetWidth;
    setParams(containerWidth);
}

function setParams(w) {
    if (w < 551) {
        slidesPerPage = 1;
    } else if (w < 901) {
        slidesPerPage = 2;
    } else if (w < 1101) {
        slidesPerPage = 3;
    } else {
        slidesPerPage = 4;
    }

    console.log("Slides: " + slides)
    console.log("Slide Per Page: " + slidesPerPage)

    slidesCount = Math.ceil(slides / slidesPerPage); // Alterado para arredondar para cima

    console.log("Slide Count = Slides / Slides Per Page ====" + slidesCount)
    // Atualiza o valor de currentPosition caso esteja fora dos limites
    if (currentPosition > slidesCount) {
        currentPosition = slidesCount - 1;
    }


    console.log("Current Position: " + currentPosition)
    console.log("Current Margin 1: " + currentMargin)

    currentMargin = - currentPosition * (100 / slidesPerPage);
    console.log("Current Margin 1: " + currentMargin)

    slider.style.marginLeft = currentMargin + '%';

    // Verifica se a posição atual é maior que 0 para habilitar o botão "left"
    if (currentPosition > 0) {
        console.log("A")
        buttons[1].classList.remove('inactive');
    } else {
        buttons[1].classList.add('inactive');
        console.log("B")
    }

    // Verifica se a posição atual é menor que a contagem de slides para habilitar o botão "right"
    if (currentPosition < slidesCount - 1) {
        buttons[0].classList.remove('inactive');
        console.log("C")
    } else {
        buttons[0].classList.add('inactive');
        console.log("D")
    }
}

function slideRight() {
    if (currentPosition !== 0 && !buttons[1].classList.contains('inactive')) { // Alterado para buttons[1]
        slider.style.marginLeft = currentMargin + (100 / slidesPerPage) + '%';
        currentMargin += (100 / slidesPerPage);
        currentPosition--;
        console.log("U");
        console.log("Current Position (pos btn erq): " + currentPosition);
    }

    if (currentPosition === 0) {
        buttons[1].classList.add('inactive'); // Alterado para buttons[1]
        buttons[0].classList.remove('inactive'); // Alterado para buttons[0]
        console.log("V");
        console.log("Current Position (pos btn erq): " + currentPosition);
    }
}

function slideLeft() {
    if (currentPosition !== slidesCount && !buttons[0].classList.contains('inactive')) { // Alterado para buttons[0]
        slider.style.marginLeft = currentMargin - (100 / slidesPerPage) + '%';
        currentMargin -= (100 / slidesPerPage);
        currentPosition++;
        console.log("X");
        console.log("Current Position (pos btn dir): " + currentPosition);
    }

    if (currentPosition === slidesCount) {
        buttons[0].classList.add('inactive'); // Alterado para buttons[0]
        console.log("Y");
        console.log("Current Position (pos btn dir): " + currentPosition);
    }

    if (currentPosition > 0) {
        buttons[1].classList.remove('inactive'); // Alterado para buttons[1]
        console.log("Z");
        console.log("Current Position (pos btn dir): " + currentPosition);
    }
}
